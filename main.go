package main

import (
	"context"
	"log"

	"gitlab.com/o-cloud/service-provider/common"
	"gitlab.com/o-cloud/service-provider/config"
	"gitlab.com/o-cloud/service-provider/provider"

	"github.com/gin-gonic/gin"
	"golang.org/x/sync/errgroup"
)

func main() {
	config.Load()
	common.LoadMongoClient()

	g, ctx := errgroup.WithContext(context.Background())

	g.Go(provider.CheckCatalogRegistration)
	g.Go(startApiServer)

	<-ctx.Done()
	log.Println("Unable to recover from error. Program shutdown")
}

func startApiServer() error {
	r := setupRouter(config.Config.Server.ApiPrefix)

	err := r.Run(config.Config.Server.ListenAddress)
	if err != nil {
		log.Fatal(err)
	}
	return err
}

func setupRouter(apiPrefix string) *gin.Engine {
	r := gin.Default()

	prefixgroup := r.Group(apiPrefix)

	provider.NewProviderHandler().SetupRoutes(prefixgroup.Group("/services"))
	return r
}
