[![pipeline status](https://gitlab.com/o-cloud/service-provider/badges/master/pipeline.svg)](https://gitlab.com/o-cloud/service-provider/commits/master)
[![coverage report](https://gitlab.com/o-cloud/service-provider/badges/master/coverage.svg)](https://gitlab.com/o-cloud/service-provider/commits/master)

# Project Service Provider API

Service provider allow client to manage cluster's services. Services are automatically registered in catalog.  

## Prerequisites

- `helm`
- `skaffold`
- `kubectl`

> You'll need an active kubectl context to use this API in skaffold

## Usage

### Run

```bash
skaffold run
```

Option *default-repo* need to be added in skaffold command for specify container registry to used.

### Dev

```bash
skaffold dev
```

## Test

From the project root, run :

```bash
go test ./... -cover
```

## Configuration

### Server

| Name          | Type   | Default |
|---------------|--------|---------|
| listenAddress | string | ":9090"
| apiPrefix     | string | "/api/discovery

### Environment variables

| Name           | Type   | Default     |
|----------------|--------|-------------|
| MONGO_DATABASE | string | "service-provider"
| CATALOG_URL    | string | "http://catalog:9091/api/catalog/internal"

## Routes

| Method | Route |
|--------|-------|
| GET    | [*prefix*/services](#get-services)
| GET    | [*prefix*/services/:name/:version](#get-servicesnameversion)
| POST   | [*prefix*/services](#post-services)
| PUT    | [*prefix*/services/:name/:version](#put-servicesnameversion)
| DELETE | [*prefix*/services/:name/:version](#delete-servicesnameversion)

### GET /services

List all the created services in the cluster.  

Response :

| Field                | Type             | Description |
|----------------------|------------------|-------------|
| services             | array of service | array of services
| \\____ name          | string           | name of the service
| \\____ description   | string           | description of the service
| \\____ version       | string           | version of the service
| \\____ cwl           | string           | cwl that describe how to use the service in pipeline
| \\____ images        | array of string  | illustation images of service
| \\____ created_at    | string           | creation date of the service
| \\____ updated_at    | string           | last update date of the service
| \\____ registered_at | string           | registration date of the service in catalog
| services_count       | number           | number of services

Response example:

```json
{
    "services": [
        {
            "name": "service name",
            "description": "service description",
            "version": "1.00",
            "cwl": "cwl",
            "images": ["https://unsplash.com/photos/TJwAiRSxp5U"],
            "created_at": "2021-06-16T08:19:21.763Z",
            "updated_at": "2021-06-16T08:19:21.763Z",
            "registered_at": "2021-06-16T08:19:25.503Z"
        }
    ],
    "services_count": 1
}
```

Return codes :

| HTTP code | Internal code | Description |
|-----------|---------------|-------------|
| 200       | /             | Success

### GET /services/:name/:version

Get the created service with requested name and version.  

Parameter :

| Name    | Type   | Required | Description |
|---------|--------|----------|-------------|
| name    | string | Yes      | name of the requested service
| version | string | Yes      | version of the requested service

Response :

| Field                | Type            | Description |
|----------------------|-----------------|-------------|
| service              | object          | /
| \\____ name          | string          | name of the service
| \\____ description   | string          | description of the service
| \\____ version       | string          | version of the service
| \\____ cwl           | string          | cwl that describe how to use the service in pipeline
| \\____ images        | array of string | illustation images of service
| \\____ created_at    | string          | creation date of the service
| \\____ updated_at    | string          | last update date of the service
| \\____ registered_at | string          | registration date of the service in catalog

Response example:

```json
{
    "service": {
        "name": "service name",
        "description": "service description",
        "version": "1.00",
        "cwl": "cwl",
        "images": ["https://unsplash.com/photos/TJwAiRSxp5U"],
        "created_at": "2021-06-16T08:19:21.763Z",
        "updated_at": "2021-06-16T08:19:21.763Z",
        "registered_at": "2021-06-16T08:19:25.503Z"
    }
}
```

Return codes :

| HTTP code | Internal code | Description |
|-----------|---------------|-------------|
| 200       | /             | Success
| 404       | 404001        | Requested service not found

### POST /services

Create a service. Combination of name and version must be unique.  

Body :

| Field              | Type            | Required | Description |
|--------------------|-----------------|----------|-------------|
| service            | object          | Yes      | /
| \\____ name        | string          | Yes      | name of the service
| \\____ description | string          | No       | description of the service
| \\____ version     | string          | Yes      | version of the service
| \\____ cwl         | string          | Yes      | cwl that describe how to use the service in pipeline
| \\____ images      | array of string | No       | illustation images of service, max 10

Response : No content

Body example:

```json
{
    "service": {
        "name": "service name",
        "description": "service description",
        "version": "1.00",
        "cwl": "cwl",
        "images": ["https://unsplash.com/photos/TJwAiRSxp5U"]
    }
}
```

Return codes :

| HTTP code | Internal code | Description |
|-----------|---------------|-------------|
| 201       | /             | Created
| 400       | 400001        | Unable to parse the JSON body
| 422       | 422001        | Unable to create the service ; required field(s) may be absent
| 422       | 422002        | service already exists

### PUT /services/:name/:version

Update an existing service with name and version requested. Combination of name and version in body must be unique.  

Parameter :

| Name    | Type   | Required | Description |
|---------|--------|----------|-------------|
| name    | string | Yes      | name of the requested service
| version | string | Yes      | version of the requested service

Body :

| Field              | Type            | Required | Description |
|--------------------|-----------------|----------|-------------|
| service            | object          | Yes      | /
| \\____ name        | string          | Yes      | name of the service
| \\____ description | string          | No       | description of the service
| \\____ version     | string          | Yes      | version of the service
| \\____ cwl         | string          | Yes      | cwl that describe how to use the service in pipeline
| \\____ images      | array of string | No       | illustation images of service, max 10

Response : No content

Body example:

```json
{
    "service": {
        "name": "service name",
        "description": "service description",
        "version": "1.00",
        "cwl": "cwl",
        "images": ["https://unsplash.com/photos/TJwAiRSxp5U"]
    }
}
```

Return codes :

| HTTP code | Internal code | Description |
|-----------|---------------|-------------|
| 200       | /             | Updated
| 400       | 400001        | Unable to parse the JSON body
| 404       | 404001        | Requested service not found
| 422       | 422001        | Unable to create the service ; required field(s) may be absent
| 422       | 422002        | service already exists

### DELETE /services/:name/:version

Delete the service with requested name and version.  

Parameter :

| Name    | Type   | Required | Description |
|---------|--------|----------|-------------|
| name    | string | Yes      | name of the requested service
| version | string | Yes      | version of the requested service

Response : no response

Return codes :

| HTTP code | Internal code | Description |
|-----------|---------------|-------------|
| 204       | /             | Deleted
| 404       | 404001        | Requested service not found
