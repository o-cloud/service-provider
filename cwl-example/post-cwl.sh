#!/bin/sh
cd "$(dirname "$0")"

if [ $# -eq 0 ] 
then
    url=http://localhost:8000/api/service-provider/services
else
    url=$1
fi


cwl=`sed -e :a -e '$!N;s/\n/\\\n/;ta' cwl_image_converter.yml`
cwl=${cwl//\"/\\\"}
echo "$cwl"
#cwlVersion: v1.0\nclass: ServiceProvider\nlabel: Converts an image\nrequirements:\n  DockerRequirement:\n    dockerPull: geodata/gdal\nbaseCommand: gdal_translate\narguments: [\"-of\"]\ninputs:\n  outputFormat:\n    inputType: definition\n    type: string\n    inputBinding:\n      position: 0\n  inputFile:\n    inputType: pipeline\n    type: File\n    inputBinding:\n      position: 1\n  outputName:  # Optionel \n    inputType: execution\n    default: image.jpeg\n    type: string\n    inputBinding:\n      position: 2\noutputs:\n  convertedFile:\n    type: File\n    outputBinding:\n      glob: $(inputs.outputName)

body="{
    \"service\": {
		\"name\":\"image_converter\",
		\"version\":\"1.0\",
		\"description\":\"Converts an image\",
		\"cwl\": \"$cwl\"
    }
}"
echo "$body"

curl -d "$body" -H "Content-Type: application/json" -X POST $url
