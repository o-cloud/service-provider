package config

import (
	"log"

	"github.com/spf13/viper"
)

var (
	Config ServiceConfig
)

const (
	dockerConfigPath = "/etc/irtsb/"
	localConfigPath  = "./"
)

func Load() {

	viper.SetConfigType("yaml")
	viper.AddConfigPath(dockerConfigPath)
	viper.AddConfigPath(localConfigPath)

	// Find and read the config file
	viper.SetConfigName("config.yaml")
	if err := viper.ReadInConfig(); err != nil {
		log.Fatal("Unable to read config file", err)
	}

	// Decode config into ServiceConfig
	if err := viper.Unmarshal(&Config); err != nil {
		log.Fatal("unable to decode configuration", err)
	}

	//Environment variables
	viper.SetDefault("MONGO_CON_STRING", "mongodb://localhost:27017/")
	viper.SetDefault("MONGO_DATABASE", "service-provider")
	viper.BindEnv("MONGO_CON_STRING")
	viper.BindEnv("MONGO_DATABASE")

	viper.SetDefault("CATALOG_URL", "http://catalog:9091/api/catalog/internal")
	viper.BindEnv("CATALOG_URL")
}

type ServiceConfig struct {
	Server ServerServiceConfig
}

type ServerServiceConfig struct {
	ListenAddress string
	ApiPrefix     string
}
