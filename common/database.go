package common

import (
	"context"
	"time"

	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var MongoClient *mongo.Collection

func LoadMongoClient() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(viper.GetString("MONGO_CON_STRING")))
	if err != nil {
		panic("Error init MongoDB client: " + err.Error())
	}

	MongoClient = client.Database(viper.GetString("MONGO_DATABASE")).Collection("services")

	createIndex()
}

func createIndex() {
	_, err := MongoClient.Indexes().CreateOne(
		context.Background(),
		mongo.IndexModel{
			Keys:    bson.D{{Key: "name", Value: 1}, {Key: "version", Value: 1}},
			Options: options.Index().SetUnique(true),
		},
	)

	if err != nil && !mongo.IsDuplicateKeyError(err) {
		panic("Error init index: " + err.Error())
	}
}

// Using this function to get a connection, you can create your connection pool here.
func GetMongoClient() *mongo.Collection {
	return MongoClient
}
