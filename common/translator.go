package common

import (
	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
)

var (
	trans *ut.Translator
)

func init() {
	english := en.New()
	uni := ut.New(english, english)
	translator, _ := uni.GetTranslator("en")
	trans = &translator
}

func GetTranslator() *ut.Translator {
	return trans
}
