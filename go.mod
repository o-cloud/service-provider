module gitlab.com/o-cloud/service-provider

go 1.16

require (
	github.com/gin-gonic/gin v1.7.2
	github.com/go-playground/locales v0.13.0
	github.com/go-playground/universal-translator v0.17.0
	github.com/go-playground/validator/v10 v10.6.1
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/spf13/viper v1.8.1
	gitlab.com/o-cloud/catalog v0.0.0-20211021092218-21ae10825add
	gitlab.com/o-cloud/cwl v0.0.0-20211021091742-30f22b2b3879
	go.mongodb.org/mongo-driver v1.5.2
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
)
