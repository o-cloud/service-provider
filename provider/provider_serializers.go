package provider

import (
	"time"

	"github.com/gin-gonic/gin"
)

type ServiceResponse struct {
	Name         string    `json:"name" binding:"required"`
	Description  string    `json:"description"`
	Version      string    `json:"version" binding:"required"`
	Cwl          string    `json:"cwl" binding:"required"`
	Images       []string  `json:"images"`
	CreatedAt    time.Time `json:"created_at" binding:"required"`
	UpdatedAt    time.Time `json:"updated_at" binding:"required"`
	RegisteredAt time.Time `json:"registered_at"`
}

type ServicesSerializer struct {
	C        *gin.Context
	Services []Service
}

type ServiceSerializer struct {
	C       *gin.Context
	Service Service
}

func (s *ServicesSerializer) Response() []ServiceResponse {
	response := []ServiceResponse{}
	for _, service := range s.Services {
		serializer := ServiceSerializer{s.C, service}
		response = append(response, serializer.Response())
	}
	return response
}

func (s *ServiceSerializer) Response() ServiceResponse {
	images := []string{}
	if len(s.Service.Images) > 0 {
		images = s.Service.Images
	}
	return ServiceResponse{
		Name:         s.Service.Name,
		Description:  s.Service.Description,
		Version:      s.Service.Version,
		Cwl:          s.Service.Cwl,
		Images:       images,
		CreatedAt:    s.Service.CreatedAt,
		UpdatedAt:    s.Service.UpdatedAt,
		RegisteredAt: s.Service.RegisteredAt,
	}
}
