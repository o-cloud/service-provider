package provider

import (
	"context"
	"encoding/base64"
	"fmt"
	"log"
	"time"

	"github.com/spf13/viper"
	catalog "gitlab.com/o-cloud/catalog/api/intern"
	providerType "gitlab.com/o-cloud/catalog/api/public"
)

func Announce(service *Service) error {

	catalogClient := catalog.NewClient(viper.GetString("CATALOG_URL"))

	sEnc := base64.StdEncoding.EncodeToString([]byte(service.Cwl))

	providerInfo := catalog.ProviderInternalInfo{
		ProviderId:        service.Id.Hex(),
		Type:              providerType.Service,
		Version:           service.Version,
		Name:              service.Name,
		Description:       service.Description,
		Metadata:          map[string]interface{}{"images": service.Images},
		WorkflowStructure: sEnc,
	}
	return catalogClient.RegisterDataProvider(context.TODO(), providerInfo)
}

func Unannounce(providerId string) error {
	catalogClient := catalog.NewClient(viper.GetString("CATALOG_URL"))
	return catalogClient.RemoveProvider(context.TODO(), providerType.Service, providerId)
}

func CheckCatalogRegistration() (err error) {
	defer func() {
		if r := recover(); r != nil {
			var ok bool
			err, ok = r.(error)
			if !ok {
				err = fmt.Errorf("pkg: %v", r)
			}
			log.Fatal(err)
		}
	}()

	log.Println("Check catalog registration running...")

	serviceprovider := GetServiceProvider()
	for {
		// Wait five seconds
		time.Sleep(5 * time.Second)

		// Get all services to sync
		services := serviceprovider.ListServicesToSync()

		for _, service := range *services {
			switch toregister := service.ToCatalogRegister; toregister {
			case ToRegister:
				// Register service in catalog
				if err = Announce(&service); err != nil {
					handleRegistrationError(err)
					continue
				}
				// Declare service registered in provider
				if err = serviceprovider.DeclareServiceRegistered(service.Id); err != nil {
					handleRegistrationError(err)
					continue
				}
			case ToUnregister:
				// Unregister service in catalog
				if err = Unannounce(service.Id.Hex()); err != nil {
					handleRegistrationError(err)
					continue
				}
				// Delete service in provider
				if err = serviceprovider.DeleteServiceByID(service.Id); err != nil {
					handleRegistrationError(err)
					continue
				}
			}
		}
	}
}

func handleRegistrationError(err error) {
	log.Println(err)
	time.Sleep(5 * time.Second)
}
