package provider_test

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/o-cloud/service-provider/common"
	"gitlab.com/o-cloud/service-provider/provider"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type testSetup struct {
	t      *testing.T
	engine *gin.Engine
	msp    *MockServiceProvider
	req    *http.Request
	rr     *httptest.ResponseRecorder
}

func getSetup(t *testing.T, method, url string, body interface{}) testSetup {

	req, err := http.NewRequest(method, url, nil)
	if body != nil {
		byts, _ := json.Marshal(body)
		req, err = http.NewRequest(method, url, bytes.NewBuffer(byts))
	}
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	r := gin.Default()
	msp := &MockServiceProvider{}
	(&provider.ProviderHandler{SP: msp}).SetupRoutes(r.Group("/services"))
	return testSetup{
		t:      t,
		engine: r,
		msp:    msp,
		req:    req,
		rr:     rr,
	}
}

func TestHandleListServices(t *testing.T) {
	services := make([]provider.Service, 2)
	services[0] = provider.Service{
		Name:        "service-name",
		Description: "service-description",
		Version:     "service-version",
		Cwl:         "service-cwl",
		Images:      []string{"http://service-image"},
	}
	services[1] = provider.Service{
		Name:        "service-name-2",
		Description: "service-description-2",
		Version:     "service-version-2",
		Cwl:         "service-cwl-2",
	}

	test := getSetup(t, "GET", "/services", nil)
	test.msp.retArray = services
	test.run()
	test.expectMethodCall(`ListServices()`)
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent(`{"services":[{"name":"service-name","description":"service-description","version":"service-version","cwl":"service-cwl","images":["http://service-image"],"created_at":"0001-01-01T00:00:00Z","updated_at":"0001-01-01T00:00:00Z","registered_at":"0001-01-01T00:00:00Z"},{"name":"service-name-2","description":"service-description-2","version":"service-version-2","cwl":"service-cwl-2","images":[],"created_at":"0001-01-01T00:00:00Z","updated_at":"0001-01-01T00:00:00Z","registered_at":"0001-01-01T00:00:00Z"}],"services_count":2}`)
}

func TestHandleGetService(t *testing.T) {
	service := provider.Service{
		Name:        "service-name",
		Description: "service-description",
		Version:     "service-version",
		Cwl:         "service-cwl",
		Images:      []string{"http://service-image"},
	}

	test := getSetup(t, "GET", "/services/"+service.Name+"/"+service.Version, nil)
	test.msp.retArray = []provider.Service{service}
	test.run()
	test.expectMethodCall(`GetService(` + service.Name + `,` + service.Version + `)`)
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent(`{"service":{"name":"service-name","description":"service-description","version":"service-version","cwl":"service-cwl","images":["http://service-image"],"created_at":"0001-01-01T00:00:00Z","updated_at":"0001-01-01T00:00:00Z","registered_at":"0001-01-01T00:00:00Z"}}`)
}

func TestHandleGetServiceNotFound(t *testing.T) {
	test := getSetup(t, "GET", "/services/randomkey/randomkey", nil)
	test.msp.retArray = make([]provider.Service, 1)
	test.msp.err = errors.New("erreur")
	test.run()
	test.expectMethodCall(`GetService(randomkey,randomkey)`)
	test.expectResponseStatus(http.StatusNotFound)
	test.expectBodyContent(`{"errors":{"` + common.ServiceNotFoundsErrorCode + `":"` + test.msp.err.Error() + `"}}`)
}

func TestHandleDeleteService(t *testing.T) {
	service := provider.Service{
		Name:        "service-name",
		Description: "service-description",
		Version:     "service-version",
		Cwl:         "service-cwl",
		Images:      []string{"http://service-image"},
	}

	test := getSetup(t, "DELETE", "/services/"+service.Name+"/"+service.Version, nil)
	test.run()
	test.expectMethodCall(`DeleteService(` + service.Name + `,` + service.Version + `)`)
	test.expectResponseStatus(http.StatusNoContent)
	test.expectBodyContent(``)
}

func TestHandleDeleteServiceNotFound(t *testing.T) {
	test := getSetup(t, "DELETE", "/services/randomkey/randomkey", nil)
	test.msp.retArray = make([]provider.Service, 1)
	test.msp.err = errors.New("erreur")
	test.run()
	test.expectMethodCall(`DeleteService(randomkey,randomkey)`)
	test.expectResponseStatus(http.StatusNotFound)
	test.expectBodyContent(`{"errors":{"` + common.ServiceNotFoundsErrorCode + `":"` + test.msp.err.Error() + `"}}`)
}

func TestHandleCreateService(t *testing.T) {
	body := provider.ServiceModelValidator{}
	body.Service.Name = "service-name"
	body.Service.Version = "service-version"
	body.Service.Description = "service-description"
	body.Service.Cwl = "cwlVersion: v1.0\nclass: DataProvider\nlabel:  Sentinel\nrequirements:\n  DockerRequirement:\n    dockerPull: mwendler/wget:latest\nbaseCommand: wget\narguments: [--tries=1, image.tif]\ninputs:\n  boundingBox:\n    inputType: definition\n    type:\n      items: string\n      type: array\n  fromDate:\n    inputType: execution\n    type: string\n  toDate:\n    inputType: execution\n    type: string\n\n  url:\n    inputType: orchestrator\n    type: string\n    inputBinding:\n      position: 1\noutputs:\n  - id: downloadedFile\n    type: File\n    outputBinding:\n      glob: image.tif\n"
	body.Service.Images = []string{"http://service-image"}

	test := getSetup(t, "POST", "/services", body)

	test.run()
	test.expectMethodCall(`CreateService(` + body.Service.Name + `,` + body.Service.Version + `)`)
	test.expectResponseStatus(http.StatusCreated)
	test.expectBodyContent(``)
}

func TestHandleCreateServiceBadRequest(t *testing.T) {
	test := getSetup(t, "POST", "/services", "plop")
	test.run()
	test.expectMethodCall(``)
	test.expectResponseStatus(http.StatusBadRequest)
	test.expectBodyContent(`{"errors":{"` + common.ServiceBadRequestErrorCode + `":"bad request : unable to parse json body"}}`)
}

func TestHandleCreateServiceCwlErrorValidation(t *testing.T) {
	body := provider.ServiceModelValidator{}
	body.Service.Name = "service-name"
	body.Service.Version = "service-version"
	body.Service.Description = "service-description"
	body.Service.Cwl = "cwlVersion: v1.0\n"
	body.Service.Images = []string{"http://service-image"}

	test := getSetup(t, "POST", "/services", body)
	test.run()
	test.expectMethodCall(``)
	test.expectResponseStatus(http.StatusUnprocessableEntity)
	test.expectBodyContent(`{"errors":{"` + common.ServiceBadBodyErrorCode + `":{"Cwl":"cwl-invalid: command line tool must have a label"}}}`)
}

func TestHandleCreateServiceUrlErrorValidation(t *testing.T) {
	body := provider.ServiceModelValidator{}
	body.Service.Name = "service-name"
	body.Service.Version = "service-version"
	body.Service.Description = "service-description"
	body.Service.Cwl = "cwlVersion: v1.0\nclass: DataProvider\nlabel:  Sentinel\nrequirements:\n  DockerRequirement:\n    dockerPull: mwendler/wget:latest\nbaseCommand: wget\narguments: [--tries=1, image.tif]\ninputs:\n  boundingBox:\n    inputType: definition\n    type:\n      items: string\n      type: array\n  fromDate:\n    inputType: execution\n    type: string\n  toDate:\n    inputType: execution\n    type: string\n\n  url:\n    inputType: orchestrator\n    type: string\n    inputBinding:\n      position: 1\noutputs:\n  - id: downloadedFile\n    type: File\n    outputBinding:\n      glob: image.tif\n"
	body.Service.Images = []string{"service-image"}

	test := getSetup(t, "POST", "/services", body)
	test.run()
	test.expectMethodCall(``)
	test.expectResponseStatus(http.StatusUnprocessableEntity)
	test.expectBodyContent(`{"errors":{"` + common.ServiceBadBodyErrorCode + `":{"Images[0]":"url: Images[0] must be a valid URL"}}}`)
}

func TestHandleCreateServiceErrorValidation(t *testing.T) {
	test := getSetup(t, "POST", "/services", provider.Service{})
	test.run()
	test.expectMethodCall(``)
	test.expectResponseStatus(http.StatusUnprocessableEntity)
	test.expectBodyContent(`{"errors":{"` + common.ServiceBadBodyErrorCode + `":{"Cwl":"required: Cwl is a required field","Name":"required: Name is a required field","Version":"required: Version is a required field"}}}`)
}

func TestHandleCreateServiceAlreadyExist(t *testing.T) {
	body := provider.ServiceModelValidator{}
	body.Service.Name = "service-name"
	body.Service.Version = "service-version"
	body.Service.Description = "service-description"
	body.Service.Cwl = "cwlVersion: v1.0\nclass: DataProvider\nlabel:  Sentinel\nrequirements:\n  DockerRequirement:\n    dockerPull: mwendler/wget:latest\nbaseCommand: wget\narguments: [--tries=1, image.tif]\ninputs:\n  boundingBox:\n    inputType: definition\n    type:\n      items: string\n      type: array\n  fromDate:\n    inputType: execution\n    type: string\n  toDate:\n    inputType: execution\n    type: string\n\n  url:\n    inputType: orchestrator\n    type: string\n    inputBinding:\n      position: 1\noutputs:\n  - id: downloadedFile\n    type: File\n    outputBinding:\n      glob: image.tif\n"
	body.Service.Images = []string{"http://service-image"}

	test := getSetup(t, "POST", "/services", body)
	test.msp.err = errors.New("Erreur")
	test.run()
	test.expectMethodCall(`CreateService(` + body.Service.Name + `,` + body.Service.Version + `)`)
	test.expectResponseStatus(http.StatusUnprocessableEntity)
	test.expectBodyContent(`{"errors":{"` + common.ServiceAlreadyExistsErrorCode + `":"` + test.msp.err.Error() + `"}}`)
}

func TestHandleUpdateService(t *testing.T) {
	body := provider.ServiceModelValidator{}
	body.Service.Name = "service-name-2"
	body.Service.Version = "service-version-2"
	body.Service.Description = "service-description-2"
	body.Service.Cwl = "cwlVersion: v1.0\nclass: DataProvider\nlabel:  Sentinel\nrequirements:\n  DockerRequirement:\n    dockerPull: mwendler/wget:latest\nbaseCommand: wget\narguments: [--tries=1, image.tif]\ninputs:\n  boundingBox:\n    inputType: definition\n    type:\n      items: string\n      type: array\n  fromDate:\n    inputType: execution\n    type: string\n  toDate:\n    inputType: execution\n    type: string\n\n  url:\n    inputType: orchestrator\n    type: string\n    inputBinding:\n      position: 1\noutputs:\n  - id: downloadedFile\n    type: File\n    outputBinding:\n      glob: image.tif\n"
	body.Service.Images = []string{"http://service-image"}

	service := provider.Service{
		Name:        "service-name",
		Description: "service-description",
		Version:     "service-version",
		Cwl:         "service-cwl",
	}

	test := getSetup(t, "PUT", "/services/"+service.Name+"/"+service.Version, body)
	test.msp.retArray = []provider.Service{service}

	test.run()
	test.expectMethodCall(`GetService(` + service.Name + `,` + service.Version + `)UpdateService(` + service.Name + `,` + service.Version + `,` + body.Service.Name + `,` + body.Service.Version + `)`)
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent(``)
}

func TestHandleUpdateServiceBadRequest(t *testing.T) {
	service := provider.Service{
		Name:        "service-name",
		Description: "service-description",
		Version:     "service-version",
		Cwl:         "service-cwl",
		Images:      []string{"http://service-image"},
	}

	test := getSetup(t, "PUT", "/services/"+service.Name+"/"+service.Version, "plop")
	test.msp.retArray = []provider.Service{service}
	test.run()
	test.expectMethodCall(``)
	test.expectResponseStatus(http.StatusBadRequest)
	test.expectBodyContent(`{"errors":{"` + common.ServiceBadRequestErrorCode + `":"bad request : unable to parse json body"}}`)
}

func TestHandleUpdateServiceErrorValidation(t *testing.T) {
	service := provider.Service{
		Name:        "service-name",
		Description: "service-description",
		Version:     "service-version",
		Cwl:         "service-cwl",
		Images:      []string{"http://service-image"},
	}

	test := getSetup(t, "PUT", "/services/"+service.Name+"/"+service.Version, provider.Service{})
	test.msp.retArray = []provider.Service{service}
	test.run()
	test.expectMethodCall(``)
	test.expectResponseStatus(http.StatusUnprocessableEntity)
	test.expectBodyContent(`{"errors":{"` + common.ServiceBadBodyErrorCode + `":{"Cwl":"required: Cwl is a required field","Name":"required: Name is a required field","Version":"required: Version is a required field"}}}`)
}

func TestHandleUpdateServiceNotFound(t *testing.T) {
	body := provider.ServiceModelValidator{}
	body.Service.Name = "service-name-2"
	body.Service.Version = "service-version-2"
	body.Service.Description = "service-description-2"
	body.Service.Cwl = "cwlVersion: v1.0\nclass: DataProvider\nlabel:  Sentinel\nrequirements:\n  DockerRequirement:\n    dockerPull: mwendler/wget:latest\nbaseCommand: wget\narguments: [--tries=1, image.tif]\ninputs:\n  boundingBox:\n    inputType: definition\n    type:\n      items: string\n      type: array\n  fromDate:\n    inputType: execution\n    type: string\n  toDate:\n    inputType: execution\n    type: string\n\n  url:\n    inputType: orchestrator\n    type: string\n    inputBinding:\n      position: 1\noutputs:\n  - id: downloadedFile\n    type: File\n    outputBinding:\n      glob: image.tif\n"
	body.Service.Images = []string{"http://service-image"}

	service := provider.Service{
		Name:        "service-name",
		Description: "service-description",
		Version:     "service-version",
		Cwl:         "service-cwl",
		Images:      []string{"http://service-image"},
	}

	test := getSetup(t, "PUT", "/services/"+service.Name+"/"+service.Version, body)
	test.msp.retArray = make([]provider.Service, 1)
	test.msp.err = errors.New("Erreur")
	test.run()
	test.expectMethodCall(`GetService(` + service.Name + `,` + service.Version + `)`)
	test.expectResponseStatus(http.StatusNotFound)
	test.expectBodyContent(`{"errors":{"` + common.ServiceNotFoundsErrorCode + `":"` + test.msp.err.Error() + `"}}`)
}

//Helper func
func (ts *testSetup) run() {
	ts.engine.ServeHTTP(ts.rr, ts.req)
}

// Check the response code is what we expect.
func (ts *testSetup) expectResponseStatus(expectedCode int) {
	if ts.rr.Code != expectedCode {
		ts.t.Errorf("handler returned wrong status code: got %v want %v",
			ts.rr.Code, expectedCode)
	}
}

// Check the response body is what we expect.
func (ts *testSetup) expectBodyContent(expectedBody string) {
	if ts.rr.Body.String() != expectedBody {
		ts.t.Errorf("handler returned unexpected body: got %v want %v",
			ts.rr.Body.String(), expectedBody)
	}
}

// Check the response body is what we expect.
func (ts *testSetup) expectMethodCall(expectedMethod string) {
	if ts.msp.called != expectedMethod {
		ts.t.Errorf("expected method not called: got %v want %v",
			ts.msp.called, expectedMethod)
	}
}

//Mock calc
type MockServiceProvider struct {
	called   string
	retArray []provider.Service
	err      error
}

func (msp *MockServiceProvider) ListServices() *[]provider.Service {
	msp.called = msp.called + "ListServices()"
	return &msp.retArray
}

func (msp *MockServiceProvider) GetService(name string, version string) (*provider.Service, error) {
	msp.called = msp.called + fmt.Sprintf("GetService(%s,%s)", name, version)
	return &msp.retArray[0], msp.err
}

func (msp *MockServiceProvider) CreateService(service *provider.Service) error {
	msp.called = msp.called + fmt.Sprintf("CreateService(%s,%s)", service.Name, service.Version)
	return msp.err
}

func (msp *MockServiceProvider) DeclareServiceToDelete(name string, version string) error {
	msp.called = msp.called + fmt.Sprintf("DeleteService(%s,%s)", name, version)
	return msp.err
}

func (msp *MockServiceProvider) UpdateService(name string, version string, service *provider.Service) error {
	msp.called = msp.called + fmt.Sprintf("UpdateService(%s,%s,%s,%s)", name, version, service.Name, service.Version)
	return msp.err
}

func (msp *MockServiceProvider) DeclareServiceRegistered(id primitive.ObjectID) error {
	return nil
}

func (msp *MockServiceProvider) DeleteServiceByID(id primitive.ObjectID) error {
	return nil
}

func (msp *MockServiceProvider) ListServicesToSync() *[]provider.Service {
	return nil
}
