package provider

import (
	"fmt"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"gitlab.com/o-cloud/cwl"
	"gitlab.com/o-cloud/service-provider/common"

	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	en_translations "github.com/go-playground/validator/v10/translations/en"
)

type ServiceModelValidator struct {
	Service struct {
		Name        string   `json:"name" binding:"required,min=5,max=128"`
		Description string   `json:"description" binding:"max=512"`
		Version     string   `json:"version" binding:"required,max=128"`
		Cwl         string   `json:"cwl" binding:"required,max=10000,cwl-invalid"`
		Images      []string `json:"images" binding:"max=10,dive,url"`
	} `json:"service"`
	serviceModel Service `json:"-"`
}

func init() {
	v := binding.Validator.Engine().(*validator.Validate)
	v.RegisterValidation("cwl-invalid", func(fl validator.FieldLevel) bool {
		cwlreader := strings.NewReader(fl.Field().String())
		_, err := cwl.ParseTool(cwlreader)
		return err == nil
	})
	trans := common.GetTranslator()
	_ = en_translations.RegisterDefaultTranslations(v, *trans)
	registerFn := func(ut ut.Translator) error {
		return ut.Add("cwl-invalid", "{0}", false)
	}
	transFn := func(ut ut.Translator, fe validator.FieldError) string {
		cwlreader := strings.NewReader(fmt.Sprintf("%v", fe.Value()))
		_, err := cwl.ParseTool(cwlreader)

		tag := fe.Tag()

		t, err := ut.T(tag, err.Error())
		if err != nil {
			return fe.(error).Error()
		}
		return t
	}
	v.RegisterTranslation("cwl-invalid", *trans, registerFn, transFn)
}

func NewServiceModelValidator() ServiceModelValidator {
	return ServiceModelValidator{}
}

func (s *ServiceModelValidator) Bind(c *gin.Context) error {

	err := c.ShouldBindWith(s, binding.JSON)
	if err != nil {
		return err
	}
	s.serviceModel.Name = s.Service.Name
	s.serviceModel.Description = s.Service.Description
	s.serviceModel.Version = s.Service.Version
	s.serviceModel.Cwl = s.Service.Cwl
	s.serviceModel.Images = s.Service.Images

	return nil
}
