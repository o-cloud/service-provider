package provider

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/o-cloud/service-provider/common"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type catalogSyncStatus int

const (
	Registered catalogSyncStatus = iota
	ToRegister
	ToUnregister
)

type Service struct {
	Id primitive.ObjectID `bson:"_id,omitempty"`

	Name        string   `bson:"name,omitempty"`
	Description string   `bson:"description,omitempty"`
	Version     string   `bson:"version,omitempty"`
	Cwl         string   `bson:"cwl,omitempty"`
	Images      []string `bson:"images,omitempty"`

	ToCatalogRegister catalogSyncStatus `bson:"to_catalog_register,omitempty"`
	CreatedAt         time.Time         `bson:"created_at,omitempty"`
	UpdatedAt         time.Time         `bson:"updated_at,omitempty"`
	RegisteredAt      time.Time         `bson:"registered_at,omitempty"`
}

type IServiceProvider interface {
	GetService(name string, version string) (*Service, error)
	ListServices() *[]Service
	CreateService(service *Service) error
	DeclareServiceToDelete(name string, version string) error
	UpdateService(name string, version string, service *Service) error
	ListServicesToSync() *[]Service
	DeclareServiceRegistered(id primitive.ObjectID) error
	DeleteServiceByID(id primitive.ObjectID) error
}

type ServiceProvider struct {
	MongoClient *mongo.Collection
}

func GetServiceProvider() IServiceProvider {
	return &ServiceProvider{
		MongoClient: common.GetMongoClient(),
	}
}

func (sp *ServiceProvider) GetService(name string, version string) (*Service, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	result := sp.MongoClient.FindOne(ctx, bson.M{"name": name, "version": version})
	err := result.Err()

	if err == mongo.ErrNoDocuments {
		err = fmt.Errorf("service %s with version %s not found", name, version)
		return nil, err
	}

	if err != nil {
		panic(err)
	}

	// create a value into which the single document can be decoded
	var service Service
	err = result.Decode(&service)
	if err != nil {
		panic(err)
	}

	return &service, nil
}

func (sp *ServiceProvider) ListServices() *[]Service {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	var services []Service
	cursor, err := sp.MongoClient.Find(ctx, bson.D{{}})

	if err == mongo.ErrNoDocuments {
		return &services
	}

	if err != nil {
		panic(err)
	}

	err = cursor.All(context.TODO(), &services)
	if err != nil {
		panic(err)
	}

	return &services
}

func (sp *ServiceProvider) CreateService(service *Service) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	service.ToCatalogRegister = ToRegister
	service.CreatedAt = time.Now()
	service.UpdatedAt = time.Now()
	_, err := sp.MongoClient.InsertOne(ctx, service)
	if mongo.IsDuplicateKeyError(err) {
		err = fmt.Errorf("service %s with version %s already exists", service.Name, service.Version)
		return err
	}

	if err != nil {
		panic(err)
	}

	return nil
}

func (sp *ServiceProvider) DeclareServiceToDelete(name string, version string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	result, err := sp.MongoClient.UpdateOne(ctx, bson.M{"name": name, "version": version}, bson.M{"$set": bson.M{
		"to_catalog_register": ToUnregister, "updated_at": time.Now(),
	}})

	if result.MatchedCount == 0 {
		err = fmt.Errorf("service %s with version %s not found", name, version)
		return err
	}

	if err != nil {
		panic(err)
	}

	return nil
}

func (sp *ServiceProvider) UpdateService(name string, version string, service *Service) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	service.ToCatalogRegister = ToRegister
	service.UpdatedAt = time.Now()

	_, err := sp.MongoClient.UpdateOne(ctx, bson.M{"name": name, "version": version}, bson.D{{Key: "$set", Value: service}})

	if mongo.IsDuplicateKeyError(err) {
		err = fmt.Errorf("service %s with version %s already exists", service.Name, service.Version)
		return err
	}

	if err != nil {
		panic(err)
	}

	return nil
}

func (sp *ServiceProvider) ListServicesToSync() *[]Service {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	var services []Service
	opts := options.Find().SetLimit(100).SetSort(bson.M{"updated_at": 1})
	results, err := sp.MongoClient.Find(ctx, bson.M{"to_catalog_register": bson.M{"$ne": Registered}}, opts)

	if err == mongo.ErrNoDocuments {
		return &services
	}

	if err != nil {
		panic(err)
	}

	err = results.All(context.TODO(), &services)

	if err != nil {
		panic(err)
	}

	return &services
}

func (sp *ServiceProvider) DeclareServiceRegistered(id primitive.ObjectID) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	result, err := sp.MongoClient.UpdateByID(ctx, id, bson.M{"$set": bson.M{"to_catalog_register": Registered, "registered_at": time.Now()}})

	if result.MatchedCount == 0 {
		err = fmt.Errorf("unable to declare service %s to be registered", id.String())
		return err
	}

	if err != nil {
		panic(err)
	}

	return nil
}

func (sp *ServiceProvider) DeleteServiceByID(id primitive.ObjectID) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	result, err := sp.MongoClient.DeleteOne(ctx, bson.M{"_id": id})

	if result.DeletedCount == 0 {
		err = fmt.Errorf("service with _id %s not found", id)
		return err
	}

	if err != nil {
		panic(err)
	}

	return nil
}
