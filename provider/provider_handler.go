package provider

import (
	"errors"
	"net/http"

	"gitlab.com/o-cloud/service-provider/common"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type ProviderHandler struct {
	SP IServiceProvider
}

func NewProviderHandler() *ProviderHandler {
	return &ProviderHandler{
		SP: GetServiceProvider(),
	}
}

// SetupRoutes Define the (sub)routes handled by JSONDecoratorHandler
func (P *ProviderHandler) SetupRoutes(router *gin.RouterGroup) {
	router.GET("/:name/:version", P.handleGet)
	router.GET("", P.handleList)
	router.POST("", P.handleCreate)
	router.PUT("/:name/:version", P.handleUpdate)
	router.DELETE("/:name/:version", P.handleDelete)

}

func (P *ProviderHandler) handleGet(c *gin.Context) {
	name := c.Param("name")
	version := c.Param("version")

	service, err := P.SP.GetService(name, version)
	if err != nil {
		body := common.NewError(common.ServiceNotFoundsErrorCode, err)
		c.JSON(http.StatusNotFound, body)
		return
	}

	serializer := ServiceSerializer{c, *service}
	c.JSON(http.StatusOK, gin.H{"service": serializer.Response()})
}

func (P *ProviderHandler) handleList(c *gin.Context) {
	services := P.SP.ListServices()
	serializer := ServicesSerializer{c, *services}
	c.JSON(http.StatusOK, gin.H{"services": serializer.Response(), "services_count": len(*services)})
}

func (P *ProviderHandler) handleCreate(c *gin.Context) {

	serviceModelValidator := NewServiceModelValidator()
	err := serviceModelValidator.Bind(c)
	if err != nil {
		errs, ok := err.(validator.ValidationErrors)
		if !ok {
			body := common.NewError(common.ServiceBadRequestErrorCode, errors.New("bad request : unable to parse json body"))
			c.JSON(http.StatusBadRequest, body)
			return
		}
		body := common.NewValidatorError(common.ServiceBadBodyErrorCode, errs)
		c.JSON(http.StatusUnprocessableEntity, body)
		return
	}

	err = P.SP.CreateService(&serviceModelValidator.serviceModel)
	if err != nil {
		body := common.NewError(common.ServiceAlreadyExistsErrorCode, err)
		c.JSON(http.StatusUnprocessableEntity, body)
		return
	}

	c.Status(http.StatusCreated)
}

func (P *ProviderHandler) handleUpdate(c *gin.Context) {
	name := c.Param("name")
	version := c.Param("version")

	serviceModelValidator := NewServiceModelValidator()
	err := serviceModelValidator.Bind(c)
	if err != nil {
		errs, ok := err.(validator.ValidationErrors)
		if !ok {
			body := common.NewError(common.ServiceBadRequestErrorCode, errors.New("bad request : unable to parse json body"))
			c.JSON(http.StatusBadRequest, body)
			return
		}
		body := common.NewValidatorError(common.ServiceBadBodyErrorCode, errs)
		c.JSON(http.StatusUnprocessableEntity, body)
		return
	}

	_, err = P.SP.GetService(name, version)
	if err != nil {
		body := common.NewError(common.ServiceNotFoundsErrorCode, err)
		c.JSON(http.StatusNotFound, body)
		return
	}

	err = P.SP.UpdateService(name, version, &serviceModelValidator.serviceModel)
	if err != nil {
		body := common.NewError(common.ServiceAlreadyExistsErrorCode, err)
		c.JSON(http.StatusUnprocessableEntity, body)
		return
	}

	c.Status(http.StatusOK)
}

func (P *ProviderHandler) handleDelete(c *gin.Context) {
	name := c.Param("name")
	version := c.Param("version")

	err := P.SP.DeclareServiceToDelete(name, version)
	if err != nil {
		body := common.NewError(common.ServiceNotFoundsErrorCode, err)
		c.JSON(http.StatusNotFound, body)
		return
	}

	c.Status(http.StatusNoContent)
}
